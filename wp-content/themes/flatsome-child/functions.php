<?php
// Add custom Theme Functions here
add_action('wp_enqueue_scripts', 'custom_scripts');
function custom_scripts(){
	wp_enqueue_script('custom-cookie-script', get_stylesheet_directory_uri() . '/jquery.cookie.js?t='.time(), array(), true, true);
	wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/custom.js?t='.time(), array(), true, true);
	wp_enqueue_style('custom-css', get_stylesheet_directory_uri() . '/custom.css?t='.time());
	
}
function custom_woocommerce_tag_cloud_widget() {
    $args = array(
        'number' => 7,
        'taxonomy' => 'product_tag'
    );
    return $args;
}
add_filter( 'woocommerce_product_tag_cloud_widget_args', 'custom_woocommerce_tag_cloud_widget' );

add_filter( 'posts_groupby', 'my_posts_groupby' );
function my_posts_groupby($groupby) {
    global $wpdb,$wp_query;
	if ( !is_admin() && $wp_query->is_main_query() ) {
		if ($wp_query->is_search) {
			$groupby = "{$wpdb->posts}.post_title";
		}
	}
    
    return $groupby;
}




add_action( 'pre_get_posts', 'create_collection_query',999999,1 );

function create_collection_query( $query ){
	
	if ( !is_admin() && $query->is_main_query() && $query->is_archive() && is_product_category() ){
		
		$cat_id = get_queried_object_id();
		
		$is_collection 	= get_field('use_as_collection', 'product_cat_'.$cat_id,false);
		$conditions = get_fields('product_cat_'.$cat_id);
		
		
		
		
		$custom_query = array();
		if( $is_collection == 'yes' && is_array($conditions) && count( $conditions['ct_collect_conditions']) > 0 &&  $conditions['ct_collect_conditions'] ){
			$order_options = @$conditions['order'];
			global $wpdb;
			$must_match = get_field('ct_collect_must_match', 'product_cat_'.$cat_id);
			$conditions = get_fields('product_cat_'.$cat_id);
			if( count($conditions) > 0 ){
				$args['post_type'] 		= 'product';
				$args['posts_per_page'] = 12;
				$args['post_status'] 	= 'publish';	
				$args['post__in']       = array(0);
				$relation =  ($must_match == 'any') ? ' OR ' : ' AND ';	

				$sql  = "SELECT ID FROM $wpdb->posts as p
						INNER JOIN $wpdb->term_relationships as tr ON ID = tr.object_id
						INNER JOIN $wpdb->term_taxonomy as tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
						INNER JOIN $wpdb->terms as t ON t.term_id = tt.term_id
						";
				$condition_order = false;
				if( $order_options && is_array( $order_options ) && count( $order_options ) > 0 ){
					$condition_order = true;
					$order_options = $order_options[0];
				}
				
			
				
				$sql .= " WHERE post_type = 'product' AND post_status = 'publish' ";
				
				
				$sql_arr = array();
				if( is_array($conditions) && count( $conditions['ct_collect_conditions']) > 0 &&  $conditions['ct_collect_conditions'] ){
					
					foreach ($conditions['ct_collect_conditions'] as $key => $condition) {
						if( $condition['ct_collect_condition_key'] == 'title' ){
							$compare = $condition['ct_collect_condition_compare'];
							$value   = $condition['ct_collect_condition_value'];
							$relation  = $condition['ct_collect_condition_operator'];	
								switch ($compare) {
									case 'equals':
										$sql_arr[$relation]['title'][] = $relation." p.post_title = '".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['title'][] = $relation." p.post_title != '".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['title'][] = $relation." p.post_title != '".$value."'";
										break;
									case 'greater_than':
										$sql_arr[$relation]['title'][] = $relation." p.post_title > '".$value."'";
										break;
									case 'less_than':
										$sql_arr[$relation]['title'][] = $relation." p.post_title < '".$value."'";
										break;
									case 'starts_with':
										$sql_arr[$relation]['title'][] = $relation." p.post_title LIKE '".$value."%'";
										break;
									case 'ends_with':
										$sql_arr[$relation]['title'][] = $relation." p.post_title LIKE '%".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['title'][] = $relation." p.post_title != '".$value."'";
										break;
									case 'contains':
										$sql_arr[$relation]['title'][] = $relation." p.post_title LIKE '%".$value."%'";
										break;
									case 'not_contains':
										$sql_arr[$relation]['title'][] = $relation." p.post_title NOT LIKE '%".$value."%'";
										break;
									case 'not_contains':
										$sql_arr[$relation]['title'][] = $relation." p.post_title NOT LIKE '%".$value."%'";
										break;
									
									default:
										# code...
										break;
								}	
						}
						
						
						
						$cat = false;
						if( $condition['ct_collect_condition_key'] == 'categories' ){
							$cat = true;
							$sql_arr['product_cat'] = " AND tt.taxonomy = 'product_cat' ";
							$compare = $condition['ct_collect_condition_compare'];
							$value   = $condition['ct_collect_condition_value'];
							$relation  = $condition['ct_collect_condition_operator'];	
								switch ($compare) {
									case 'equals':
										$sql_arr[$relation]['term'][] = $relation." t.name = '".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['term'][] = $relation." t.name != '".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['term'][] = $relation." t.name != '".$value."'";
										break;
									case 'greater_than':
										$sql_arr[$relation]['term'][] = $relation." t.name > '".$value."'";
										break;
									case 'less_than':
										$sql_arr[$relation]['term'][] = $relation." t.name < '".$value."'";
										break;
									case 'starts_with':
										$sql_arr[$relation]['term'][] = $relation." t.name LIKE '".$value."%'";
										break;
									case 'ends_with':
										$sql_arr[$relation]['term'][] = $relation." t.name LIKE '%".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['term'][] = $relation." t.name != '".$value."'";
										break;
									case 'contains':
										$sql_arr[$relation]['term'][] = $relation." t.name LIKE '%".$value."%'";
										break;
									case 'not_contains':
										$sql_arr[$relation]['term'][] = $relation." t.name NOT LIKE '%".$value."%'";
										break;
									case 'not_contains':
										$sql_arr[$relation]['term'][] = $relation." t.name NOT LIKE '%".$value."%'";
										break;
									
									default:
										# code...
										break;
								}	
						}
						if( $condition['ct_collect_condition_key'] == 'tag' ){
							//$sql.= " AND tt.taxonomy = 'product_tag' ";
							$sql_arr['product_tag'] = " AND tt.taxonomy = 'product_tag' ";
							$compare = $condition['ct_collect_condition_compare'];
							$value   = $condition['ct_collect_condition_value'];
							$relation  = $condition['ct_collect_condition_operator'];	
								switch ($compare) {
									case 'equals':
										$sql_arr[$relation]['term'][] = $relation." t.name = '".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['term'][] = $relation." t.name != '".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['term'][] = $relation." t.name != '".$value."'";
										break;
									case 'greater_than':
										$sql_arr[$relation]['term'][] = $relation." t.name > '".$value."'";
										break;
									case 'less_than':
										$sql_arr[$relation]['term'][] = $relation." t.name < '".$value."'";
										break;
									case 'starts_with':
										$sql_arr[$relation]['term'][] = $relation." t.name LIKE '".$value."%'";
										break;
									case 'ends_with':
										$sql_arr[$relation]['term'][] = $relation." t.name LIKE '%".$value."'";
										break;
									case 'not_equals':
										$sql_arr[$relation]['term'][] = $relation." t.name != '".$value."'";
										break;
									case 'contains':
										$sql_arr[$relation]['term'][] = $relation." t.name LIKE '%".$value."%'";
										break;
									case 'not_contains':
										$sql_arr[$relation]['term'][] = $relation." t.name NOT LIKE '%".$value."%'";
										break;
									case 'not_contains':
										$sql_arr[$relation]['term'][] = $relation." t.name NOT LIKE '%".$value."%'";
										break;
									
									default:
										# code...
										break;
								}	
						}
					}
				}
				$ids = array(0);
			
				
				if( count( $sql_arr ) > 0 ){
					
					$is_seach_term_and = false;
					$sql_prepend = '';
					if( isset( $sql_arr['and'] )&& isset($sql_arr['and']['term']) && count( $sql_arr['and']['term'] ) > 0  ){
						
						
						foreach( $sql_arr['and']['term'] as $key => $sql_term ){
							/* start loop */
							$terms_post_id_results = $wpdb->get_results( $sql.''. $sql_term );
							$terms_post_ids = array();
							if( count($terms_post_id_results) > 0 ){
								foreach ($terms_post_id_results as $terms_post_id_result) {
									$terms_post_ids[] = $terms_post_id_result->ID;
								}
								
								unset(  $sql_arr['and']['term'][$key] );
								unset(  $sql_arr['product_cat'] );
								
								$terms_post_ids = implode( ",",$terms_post_ids );
								//$sql_arr['and']['term'][$key] = " AND p.ID  IN ( ". $terms_post_ids ." )";
								$sql_prepend .= " AND p.ID  IN ( ". $terms_post_ids ." )";
							}
							/* end loop */
						}
						
						
					}
					
					
					
					if( isset( $sql_arr['product_cat'] ) && isset( $sql_arr['product_tag'] ) ){
						$sql_arr['product_tag'] = str_replace( 'AND','', $sql_arr['product_tag']);
						$sql_arr['product_cat'] = str_replace( 'AND','', $sql_arr['product_cat']);
						$sql .= ' AND ('.$sql_arr['product_cat'] .' OR '.$sql_arr['product_tag'].') ';
					}elseif( isset( $sql_arr['product_cat'] ) && !isset( $sql_arr['product_tag'] ) ){
						$sql .= ' '.$sql_arr['product_cat'];
					}elseif( !isset( $sql_arr['product_cat'] ) && isset( $sql_arr['product_tag'] ) ){
						$sql .= ' '.$sql_arr['product_tag'];
					}
					
					
					
						
					$sql_arr_ors = 	array();
					foreach( $sql_arr as $sql_arr_key => $sql_arr_vals ){
						if( $sql_arr_key == 'or'){
							foreach( $sql_arr_vals as $sql_arr_val_arr ){
								foreach( $sql_arr_val_arr as $sql_arr_val_arr_2 ){
									$sql_arr_ors[] = $sql_arr_val_arr_2;
								}
								
							}
						}
					}
					foreach( $sql_arr as $sql_arr_key => $sql_arr_vals ){
						
						if( $sql_arr_key == 'and'){
							foreach( $sql_arr_vals as $sql_arr_val ){
								$sql .= implode(' ',$sql_arr_val);
							}
						}
					}
					if( count( $sql_arr_ors ) > 0 ){
						$sql .= ' AND ( ';
						foreach( $sql_arr_ors as $k => $sql_arr_val_1 ){
							if( $k == 0 ){
								$sql .= ' '.ltrim($sql_arr_val_1, "or");
							}else{
								$sql .= ' '.$sql_arr_val_1;
							}
						}
						$sql .= ' )';
					}
					
					
					$sql .= $sql_prepend;
					
				}	

				
				
				
				$sql .= ' GROUP BY p.ID';
				
				
				
				if( isset( $_GET['debug'] ) ){
					echo '<pre>';
					print_r($sql );
					echo '</pre>';
				}
				
				$results = $wpdb->get_results($sql);
				if( count($results) > 0 ){
					foreach ($results as $result) {
						$ids[] = $result->ID;
					}
				}
				$query->set('is_search',1);
				$query->set('product_cat','');
				$query->set('post__in',$ids);
				$query->set('is_archive','');
				$query->set('is_tax','');
				$query->set('orderby','ID');
				$query->set('order','DESC');
				
				if( $condition_order ){
					
					$order_direct = $order_options['order_direction'];
					switch ( $order_options['order_field'] ) {
						case 'title':
							$query->set('orderby','title');
							$query->set('order',$order_direct);
							break;
						case 'time':
							$query->set('orderby','date');
							$query->set('order',$order_direct);
							break;
						case 'best_selling':
							$query->set( 'meta_query', array(
								array(
									  'key' => 'total_sales'
								)
							));
							$query->set('orderby','meta_value_num');
							$query->set('order',$order_direct);
							break;
						default:
						
							break;
					}
					
				}
				
				
				
				
				if( isset( $_GET['collection_filter'] ) && $_GET['collection_filter'] == 1 && isset( $_GET['cl_orderby'] ) && $_GET['cl_orderby'] != "" ){
					switch ($_GET['cl_orderby']) {
						case 'rating':
							
							$query->set( 'meta_query', array(
								array(
									  'key' => '_wc_average_rating'
								)
							));
							$query->set('orderby','meta_value_num');
							$query->set('order','DESC');
							break;
						case 'price':
							$query->set( 'meta_query', array(
								array(
									  'key' => '_price'
								)
							));
							$query->set('orderby','meta_value_num');
							$query->set('order','ASC');
							break;
						case 'popularity':
							$query->set('orderby','popularity');
							$query->set('order','DESC');
							break;
						case 'price-desc':
							$query->set( 'meta_query', array(
								array(
									  'key' => '_price'
								)
							));
							$query->set('orderby','meta_value_num');
							$query->set('order','DESC');
							break;
						default:
							$query->set('orderby','ID');
							$query->set('order','DESC');
							break;
					}
				}
				
			}
		}
		
	}
			
	return $query;
}


function woocommerce_product_size_chart_tab(){
	global $post;
	$terms = get_the_terms( $post->ID, 'product_cat' );
	$cat_id = 0;
	$use_size_chart = '';

	if( count($terms) > 0 ){
		foreach ($terms as $term) {
			$cat_id = $term->term_id;
			
			$use_size_chart = get_term_meta( $cat_id , 'wpcf-image', true );
			if( $use_size_chart  ) break;
		}
	}
	if(  $use_size_chart ){
		
		echo '<img src="'.$use_size_chart.'">';
	}
}