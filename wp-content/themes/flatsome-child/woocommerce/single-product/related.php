<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see       https://docs.woocommerce.com/document/template-structure/
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

// Fallback to old
if(!fl_woocommerce_version_check('3.0.0')){
  return wc_get_template_part( 'single-product/related-old');
}

// Get Type
$type = get_theme_mod('related_products', 'slider');
if($type == 'hidden') return;
if($type == 'grid') $type = 'row';

// Disable slider if less than selected products pr row.
if ( sizeof( $related_products ) < (get_theme_mod('related_products_pr_row', 4)+1) ) {
  $type = 'row';
}

$repater['type'] = $type;
$repater['columns'] = get_theme_mod('related_products_pr_row', 4);
$repater['slider_style'] = 'reveal';
$repater['row_spacing'] = 'small';
global $post,$wpdb;
$tags_array = $cats_array = array();
$cats = wp_get_post_terms( $post->ID, "product_cat" );
foreach ( $cats as $cat ) {
	$cats_array[] = $cat->term_id;
}

$tags = wp_get_post_terms( $post->ID, "product_tag" );
foreach ( $tags as $tag ) {
	$tags_array[] = $tag->term_id;
}



$title_posts = array();
$title_results = array();

$post_titles = explode(  " " , $post->post_title );



$post_titles_tmp_number = array();
$post_titles_tmp_tex_number = array();
$post_titles_tmp_tex_str = array();
if( count( $post_titles  ) > 0 ){
	foreach( $post_titles as $post_title ){
		if( is_numeric($post_title) ){
			$post_titles_tmp_number[] = $post_title;
		}elseif(1 === preg_match('~[0-9]~', $post_title)){
			$post_titles_tmp_tex_number[] = $post_title;
		}else{
			$post_titles_tmp_tex_str[] = $post_title;
		}
	}
}
$post_titles = array_merge($post_titles_tmp_number,$post_titles_tmp_tex_number,$post_titles_tmp_tex_str);


if( count( $post_titles  ) > 0 ){
	foreach( $post_titles as $post_title ){
		$sql = "SELECT ID FROM $wpdb->posts WHERE post_type = 'product' AND post_status = 'publish' AND ID != ".$post->ID." AND post_title LIKE '%".$post_title."%'";
		$post_ids = $wpdb->get_results( $sql );
		
		if( count( $post_ids ) > 0 ){
			foreach( $post_ids as $post_id ){
				$my_post = get_post($post_id->ID);
				$title_results[$my_post->post_title] = $my_post;
			}
		}
	}
	
	$related_products =  $title_results;
}

if( count( $related_products ) < 8 ){
	$related_posts_tag = new WP_Query(
		array(

			'posts_per_page' => -1,
			'post_type' => 'product',
			'post__not_in' => array($post->ID),
			'tax_query' => array(
				array(
					'taxonomy' => 'product_tag',
					'field' => 'id',
					'terms' => $tags_array
				)
			)
		) 
	);
	$related_products +=  $related_posts_tag->posts;
}

if( count( $related_products ) < 8 ){
	$related_posts_cat = new WP_Query(
		array(
			
			'posts_per_page' => -1,
			'post_title LIKE' => $post->post_title,
			'post_type' => 'product',
			'post__not_in' => array($post->ID),
			'tax_query' => array(
				array(
					'taxonomy' => 'product_cat',
					'field' => 'id',
					'terms' => $cats_array
				)
			)
		) 
	);
	
	$related_products +=  $related_posts_cat->posts;
	
}
$new_related_products = array();
$i = 0;
foreach( $related_products as $related_product ){
	if( $i == 8 ) break;
	$new_related_products[ $related_product->post_title ] =  $related_product;
	
	$i++;
}
$related_products = $new_related_products;


if ( $related_products ) : ?>

  <div class="related related-products-wrapper product-section">

    <h3 class="product-section-title product-section-title-related pt-half pb-half uppercase">
      <?php esc_html_e( 'Related products', 'woocommerce' ); ?>
    </h3>

      <?php echo get_flatsome_repeater_start($repater); ?>

      <?php foreach ( $related_products as $related_product ) : ?>

        <?php
          $post_object = get_post( $related_product );

          setup_postdata( $GLOBALS['post'] =& $post_object );

          wc_get_template_part( 'content', 'product' ); ?>

      <?php endforeach; ?>

      <?php echo get_flatsome_repeater_end($repater); ?>

  </div>

<?php endif;

wp_reset_postdata();
