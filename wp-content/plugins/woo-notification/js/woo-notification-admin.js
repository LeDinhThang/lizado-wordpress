'use strict';
jQuery(document).ready(function () {
	jQuery('.vi-ui.tabular.menu .item').tab();
	jQuery('.vi-ui.checkbox').checkbox();
	jQuery('select.vi-ui.dropdown').dropdown();

	jQuery('input[name="wnotification_params[position]"]').on('change', function () {
		var data = jQuery(this).val();
		if (data == 1) {
			jQuery('#message-purchased').removeClass('top_left top_right').addClass('bottom_right');
		} else if (data == 2) {
			jQuery('#message-purchased').removeClass('bottom_right top_right').addClass('top_left');
		} else if (data == 3) {
			jQuery('#message-purchased').removeClass('bottom_right top_left').addClass('top_right');
		} else {
			jQuery('#message-purchased').removeClass('bottom_right top_left top_right');
		}
	});

	jQuery('.product-search').select2({
        placeholder       : "Please fill in your  product title",
        ajax              : {
            url           : "admin-ajax.php?action=wcnf_search_product",
            dataType      : 'json',
            type          : "GET",
            quietMillis   : 50,
            delay         : 250,
            data          : function (params) {
                return {
                    keyword: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache         : true
        },
        escapeMarkup      : function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1
    });
});