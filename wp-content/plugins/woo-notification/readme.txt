=== WooCommerce Notification - Boots your sales ===
Contributors: mrt3vn
Donate link: http://www.villatheme.com/donate
Tags: woocommerce, notify, marketing, boost sales ,recent sales popup,live sales,recent sales notification, villatheme, woocommerce notification, up sell
Requires at least: 4.4
Tested up to: 4.8.1
Stable tag: 1.1.3
License: GPLv2 or later

== Description ==
>[Try the Demo](http://new2new.com/?item=woocommerce-notification "Demo WooCommerce Notification") | [Documents](http://docs.villatheme.com/?item=woocommerce-notification "Documents") | [Pro Version](https://goo.gl/PwXTzT "Pro Version")

[youtube https://www.youtube.com/watch?v=DpYR4oH6RDQ]

WooCommerce Notification displays recent orders on your storefront. It’s the online equivalent of a busy store, and shows prospective customers that other people are buying your products.

- Increase conversion rate by highlighting other customers that have bought products.
- Display orders in real time for buyer validation and social proof!
- Create a sense of urgency for visitors, and expose new products!

### IMPORTANT NOTICE

- Plugin works based on WooCommerce plugin.

- It is released on WordPress.org and you can use plugin as free to build themes for sale.

### FEATURES

- **Works on mobile!** Choose how your notifications are displayed on mobile, or turn them off for mobile devices.

- **Custom message purchased** With available shortcodes, you can build content of notification of real purchased products or assumed to be purchased products. Customer shortcode available as first name, customer city, customer country, tittle product, with links tittle product, time after purchase. Just simply select shortcodes without do anything else then the system will automatically take off these information the same as your configuration.

- **Up sales with specific product** For special products such as new products, or best-selling products, or high quality products etc, you can choose the name of these products to display related information in notification.

- **Make virtual address of customer and name** Sometimes, it is not necessarily a real customer buying any product then system creates a notification. This feature allows an administrator to set virtual customer names purchased specific product with different address, in order to attract attention to a new product.

- **Set Virtual Time to auto get random** Similar to the virtual name or virtual address, you can also set the virtual time to determine how long ago someone purchased a specific virtual product.

- **Available 4 styles position to show notification**

- **SPEED** Use Ajax technology. It will load after your site display. So It isn't realated your speed site.

- **Easy to use**

###PRO VERSION

- **All features from free version.**

- **6 months Premium support**

- **Show or hide on pages** You can use “conditional tags” option to show notification on a particular page depending on what conditions that page matches. Simply indicate ID or tittle of page that you want to show notification. For Example, is_page(‘About’) or is_page(50) or is_page( array(‘About’, ‘Contact’, 50, 75) ).

- **Custom message checkout.**

- **Get real products from order what is processing or completed** Allows system to select products from billing to show information related to real purchased products such as name of customer, name of purchased product, address of customer.. from bill to show in front-end of form notification.

- **Set time threshold to get recent product from order** with option “Order Time” allows to configure time to products get from order. Mean that purchased products will be randomly displayed in notification form, just show the products are purchased repeatedly since it has been purchased until the end of time “Order Time”.

- **Auto detect real address with ipFind AP** By intergrating ipfind into WooCommerce Notification plugin, you can simply fetch an end point with the IP and receive an instant JSON response with the location data you need.

- **ipFind auth key** after have sign up and enter your key into field “ipFind auth key”, system will auto detect your IP address for notification.

- **Unlimited color of highlight, text, background** WooCommer Notification plugin provides unlimited colors and skins to help you configure color of highlight, text, background.

- **Available 4 styles position, 2 image positions** to show notification and more on demand.

- **Sound effect when show notification** you can choose an available audio so that the notification will be appeared along with an audio in front-end.

- **Set time** to display notification, delay time to wait from showing, loop time.

- **Save Logs** Save logs helps system stores information when visitors click on notification. Then admin site could be statistic of number clicks and analysis fluctuation sales. Report system allows to statistic number clicks by date or by product.

- **See users interact with your notifications in real time** with featured save logs, admin site will learn more about attitudes and behavior of customer for product showed on notification.

- [GET PRO VERSION](https://goo.gl/PwXTzT) or [http://codecanyon.net/item/woocommerce-notification-boost-your-sales/16586926](https://goo.gl/PwXTzT)

### Documentation

- [Getting Started](http://docs.villatheme.com/?item=woocommerce-notification)

### Plugin Links

- [Project Page](https://villatheme.com)
- [Documentation](http://docs.villatheme.com/?item=woocommerce-notification)
- [Report Bugs/Issues](https://villatheme.com/supports)

== Frequently Asked Questions ==

== Upgrade Notice ==

== Installation ==

1. Unzip the download package
1. Upload `woo-notification` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==
1. Settings
2. Front end
== Changelog ==
/**1.1.3 - 2017.09.06*/
- Optimized: Request db
- Fixed: Ajax search product with big data

/**1.1.2.1- 2017.09.06**/
- Optimize: Show on front page without Session

/**1.1.2 - 2017.8.20**/
- Fixed: Compatible with WooCommerce 3.0.0, higher and lower
- Optimize: Get option, Ads

/**1.1.1 - 2017.6.28**/
- Fixed: No sound on mobile

/**1.1 - 2017.2.17**/
- Fixed: Link
- Fixed: Get Product variation

/**1.0.3 - 2017.2.17**/
- Fixed: Admin menu hover

/**1.0.2 - 2017.2.10**/
- Fixed: Save button in backend
- Fixed: Responsive on mobile
- Fixed: Product image thumb

/**1.0.1 - 09.14.2016**/
- Fixed: Conflict JS in Admin setting
- Fixed: Ad Session
- Added: Update Pro version
- Added: Language translate

/**1.0.0 - 07.13.2016**/
- The first released