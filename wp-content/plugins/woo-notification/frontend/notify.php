<?php

/**
 * Class VI_WNOTIFICATION_F_Frontend_Notify
 */
class VI_WNOTIFICATION_F_Frontend_Notify {
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'init_scripts' ) );
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );

		add_action( 'wp_ajax_nopriv_woonotification_get_product', array( $this, 'product_html' ) );
		add_action( 'wp_ajax_woonotification_get_product', array( $this, 'product_html' ) );

		/*WordPress lower 4.5*/
		if ( woocommerce_notification_wpversion() ) {
			add_action( 'wp_print_scripts', array( $this, 'custom_script' ) );
		}
	}

	public function custom_script() {
		$script = 'var wnotification_ajax_url = "' . admin_url( 'admin-ajax.php' ) . '"'; ?>
		<script type="text/javascript">
			<?php echo $script; ?>
		</script>
	<?php }

	/**
	 * Show HTML on front end
	 */
	public function product_html() {
		$params = new VI_WNOTIFICATION_F_Admin_Settings();
		$enable = $params->get_field( 'enable' );
		if ( $enable ) {
			echo $this->show_product();
		}
		die;
	}

	/**
	 * Show HTML code
	 */
	public function wp_footer() {
		$params = new VI_WNOTIFICATION_F_Admin_Settings();
		$enable = $params->get_field( 'enable' );
		if ( $enable ) {
			echo $this->show_product();
		}
	}

	/**
	 * Add Script and Style
	 */
	function init_scripts() {
		wp_enqueue_style( 'woo-notification', VI_WNOTIFICATION_F_CSS . 'woo-notification.css', array(), VI_WNOTIFICATION_F_VERSION );
		wp_enqueue_script( 'woo-notification', VI_WNOTIFICATION_F_JS . 'woo-notification.js', array( 'jquery' ), VI_WNOTIFICATION_F_VERSION );

	}

	/**
	 * Show product
	 *
	 * @param $product_id Product ID
	 *
	 */
	protected function show_product() {
		$params                = new VI_WNOTIFICATION_F_Admin_Settings();
		$image_position        = $params->get_field( 'image_position' );
		$position              = $params->get_field( 'position' );
		$loop                  = $params->get_field( 'loop', 0 );
		$initial_delay         = $params->get_field( 'initial_delay', 5 );
		$notification_per_page = $params->get_field( 'notification_per_page', 30 );
		$display_time          = $params->get_field( 'display_time', 5 );
		$next_time             = $params->get_field( 'next_time', 60 );
		$class                 = array();
		$class[]               = $image_position ? 'img-right' : '';
		switch ( $position ) {
			case  1:
				$class[] = 'bottom_right';
				break;
			case  2:
				$class[] = 'top_left';
				break;
			case  3:
				$class[] = 'top_right';
				break;
		}
		ob_start();
		?>
		<div id="message-purchased" class="customized <?php echo implode( ' ', $class ) ?>" style="display: none;"
			 data-loop="<?php echo esc_attr( $loop ) ?>"
			 data-initial_delay="<?php echo esc_attr( $initial_delay ) ?>"
			 data-notification_per_page="<?php echo esc_attr( $notification_per_page ) ?>"
			 data-display_time="<?php echo esc_attr( $display_time ) ?>"
			 data-next_time="<?php echo esc_attr( $next_time ) ?>">

			<?php echo $this->message_purchased() ?>

		</div>
		<?php
		return ob_get_clean();
	}

	/**
	 * Message purchased
	 *
	 * @param $product_id
	 */
	protected function message_purchased() {
		$params            = new VI_WNOTIFICATION_F_Admin_Settings();
		$message_purchased = $params->get_field( 'message_purchased' );
		$show_close_icon   = $params->get_field( 'show_close_icon' );

		$messsage = '';
		$keys     = array(
			'{first_name}',
			'{city}',
			'{country}',
			'{product}',
			'{product_with_link}',
			'{time_ago}'
		);


		$product = $this->get_product();
		if ( $product ) {
			$product_id = $product['id'];
		} else {
			return false;
		}

		$first_name = $product['first_name'];
		$city       = $product['city'];
		$country    = $product['country'];
		$time       = $product['time'];
		if ( woocommerce_version_check() ) {
			$product = esc_html( get_the_title( $product_id ) );
		} else {
			$variabl       = wc_get_product( $product_id );
			$prd_var_title = $variabl->post->post_title;
			if ( $variabl->get_type() == 'variation' ) {
				$prd_var_attr = $variabl->get_variation_attributes();
				$attr_name1   = array_values( $prd_var_attr )[0];
				$product      = $prd_var_title . ' - ' . $attr_name1;
			} else {
				$product = $prd_var_title;
			}
		}
		$link = get_permalink( $product_id );
		$link = wp_nonce_url( $link, 'wocommerce_notification_click', 'link' );
		ob_start(); ?>
		<a href="<?php echo esc_url( $link ) ?>"><?php echo esc_html( $product ) ?></a>
		<?php $product_with_link = ob_get_clean();
		ob_start(); ?>
		<small><?php echo esc_html__( 'About', 'woo-notification' ) . ' ' . esc_html( $time ) . ' ' . esc_html__( 'ago', 'woo-notification' ) ?></small>
		<?php $time_ago = ob_get_clean();

		if ( has_post_thumbnail( $product_id ) ) {
			$messsage .= '<img src="' . esc_url( get_the_post_thumbnail_url( $product_id, "shop_thumbnail" ) ) . '" class="wcn-product-image"/>';
		}
		$replaced = array(
			$first_name,
			$city,
			$country,
			$product,
			$product_with_link,
			$time_ago
		);
		$messsage .= str_replace( $keys, $replaced, '<p>' . strip_tags( $message_purchased ) . '</p>' );
		ob_start();
		if ( $show_close_icon ) {
			?>
			<span id="notify-close"></span>
			<?php
		}
		$messsage .= ob_get_clean();

		return $messsage;
	}

	protected function get_product() {

		$params = new VI_WNOTIFICATION_F_Admin_Settings();

		$prefix = woo_notification_prefix();
		/*Process section*/
		$cache = get_transient( $prefix );
		if ( ! is_array( get_transient( $prefix ) ) ) {
			$cache = array();
		} else {
			return $cache;
		}
		$data_cache = array_filter( $cache );
		$sec_datas  = count( $data_cache ) ? $data_cache : array();

		if ( count( $sec_datas ) ) {

			/*Process data with product up sell*/
			$index        = rand( 0, count( $sec_datas ) - 1 );
			$data         = $sec_datas[ $index ];
			$data['id']   = $sec_datas['id'];
			$virtual_time = $params->get_field( 'virtual_time' );
			$data['time'] = $this->time_substract( current_time( 'timestamp' ) - rand( 10, $virtual_time * 3600 ), true );
			/*Change virtual name*/
			$virtual_name = $params->get_field( 'virtual_name' );
			if ( $virtual_name ) {
				$virtual_name       = explode( "\n", $virtual_name );
				$virtual_name       = array_filter( $virtual_name );
				$index              = array_rand( $virtual_name );
				$virtual_name_text  = $virtual_name[ $index ];
				$data['first_name'] = $virtual_name_text;
			}
			/*Change city*/
			if ( $params->get_field( 'virtual_country' ) ) {
				$data['country'] = $params->get_field( 'virtual_country' );
				/*Change city*/
				$city = $params->get_field( 'virtual_city' );
				if ( $city ) {
					$city         = explode( "\n", $city );
					$city         = array_filter( $city );
					$index        = array_rand( $city );
					$city_text    = $city[ $index ];
					$data['city'] = $city_text;
				}
			}

			return $data;
		}

		/*Params from Settings*/
		$archive_products = $params->get_field( 'archive_products' );
		$virtual_name     = $params->get_field( 'virtual_name' );
		$virtual_time     = $params->get_field( 'virtual_time' );

		if ( $virtual_name ) {
			$virtual_name = explode( "\n", $virtual_name );
			$virtual_name = array_filter( $virtual_name );
		}
		$country = $params->get_field( 'virtual_country' );
		$city    = $params->get_field( 'virtual_city' );
		if ( $city ) {
			$city = explode( "\n", $city );
			$city = array_filter( $city );
		}

		$archive_products = $archive_products ? $archive_products : '';

		if ( ! $archive_products ) {
			$args      = array(
				'post_type'      => 'product',
				'post_status'    => 'publish',
				'posts_per_page' => '1',
				'orderby'        => 'date',
				'order'          => 'DESC'
			);
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				$archive_products = array();
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$archive_products[] = get_the_ID();
				}
			}
		}
		$products = array();
		foreach ( $archive_products as $archive_product ) {

			if ( is_array( $city ) ) {
				$index     = array_rand( $city );
				$city_text = $city[ $index ];
			} else {
				$city_text = $city;
			}

			if ( is_array( $virtual_name ) ) {
				$index             = array_rand( $virtual_name );
				$virtual_name_text = $virtual_name[ $index ];
			} else {
				$virtual_name_text = $virtual_name;
			}


			$product['id']         = $archive_product;
			$product['time']       = $this->time_substract( current_time( 'timestamp' ) - rand( 10, $virtual_time * 3600 ), true );
			$product['first_name'] = $virtual_name_text;
			$product['city']       = $city_text;
			$product['country']    = $country;
			$products[]            = $product;

			if ( count( $products ) ) {
				$data = $products[0];
				unset( $products[0] );
				$products   = array_values( $products );
				$products[] = $data;
				set_transient( $prefix, $product, 1800 );
			} else {
				return false;
			}

		}

		return $data;

	}

	/**
	 * Subtract time from current time
	 *
	 * @param $time
	 *
	 * @return bool|string
	 */
	protected function time_substract( $time, $number = false ) {
		if ( ! $number ) {
			if ( $time ) {
				$time = strtotime( $time );
			} else {
				return false;
			}
		}
		$current_time   = current_time( 'timestamp' );
		$time_substract = $current_time - $time;
		if ( $time_substract > 0 ) {

			/*Check day*/
			$day = $time_substract / ( 24 * 3600 );
			$day = intval( $day );
			if ( $day > 1 ) {
				return $day . ' ' . esc_html__( 'days', 'woo-notification' );
			} elseif ( $day > 0 ) {
				return $day . ' ' . esc_html__( 'day', 'woo-notification' );
			}

			/*Check hour*/
			$hour = $time_substract / ( 3600 );
			$hour = intval( $hour );
			if ( $hour > 1 ) {
				return $hour . ' ' . esc_html__( 'hours', 'woo-notification' );
			} elseif ( $hour > 0 ) {
				return $hour . ' ' . esc_html__( 'hour', 'woo-notification' );
			}

			/*Check min*/
			$min = $time_substract / ( 60 );
			$min = intval( $min );
			if ( $min > 1 ) {
				return $min . ' ' . esc_html__( 'minutes', 'woo-notification' );
			} elseif ( $min > 0 ) {
				return $min . ' ' . esc_html__( 'minute', 'woo-notification' );
			}

			return intval( $time_substract ) . ' ' . esc_html__( 'seconds', 'woo-notification' );

		} else {
			return false;
		}


	}

}