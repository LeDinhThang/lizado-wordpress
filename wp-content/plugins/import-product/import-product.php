<?php
/*
Plugin Name: Import Products
Plugin URI: thelifecode.net
Description: Import Products
Version: 0.0.1
Author: taitamfc
Author URI: thelifecode.net
License: GPL2
*/
//error_reporting(-1);

 define( 'CLP_ABSPATH' , trailingslashit( WP_PLUGIN_DIR . '/' . str_replace(basename( __FILE__ ) , "" , plugin_basename( __FILE__ ) ) ) );
 define( 'CLP_URI'     , trailingslashit( WP_PLUGIN_URL . '/' . str_replace(basename( __FILE__ ) , "" , plugin_basename( __FILE__ ) ) ) );

 $upload_dir = wp_upload_dir();
 define('CLP_UPLOADIR',$upload_dir['basedir'].'/clp/');

 require_once CLP_ABSPATH .'/classes/clp-helper.php';
 require_once CLP_ABSPATH .'/classes/clp-data.php';
 require_once CLP_ABSPATH .'/classes/clp-model.class.php';
 require_once CLP_ABSPATH .'/classes/clp-admin.class.php';
 require_once CLP_ABSPATH .'/classes/clp-ajax.class.php';
 
$CPL_Admin = new CPL_Admin();

add_action('init','tool_init');
function tool_init (){
 	add_filter( 'wp_get_attachment_url', 'tool_wp_get_attachment_url', 99, 2 );
	

 	/*
 	global $wpdb;
 	$sql  = "SELECT ID FROM $wpdb->posts as p
						INNER JOIN $wpdb->term_relationships as tr ON ID = tr.object_id
						INNER JOIN $wpdb->term_taxonomy as tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
						INNER JOIN $wpdb->terms as t ON t.term_id = tt.term_id
						WHERE post_type = 'product' AND post_status = 'publish' ";
	$sql .= ' GROUP BY p.ID';
				
	$results = $wpdb->get_results($sql);
	if( count($results) > 0 ){
		foreach ($results as $result) {
			$pro_id = $result->ID;
			update_post_meta( $pro_id, '_price', get_post_meta( $pro_id,'custom_price',true) );
		}
	}
	die();
	*/
 	
}

function tool_wp_get_attachment_url( $url, $post_id ){
	if( get_post_meta($post_id,'is_cdn',true) ){
		$url = get_post_meta( $post_id, '_wp_attached_file', true );

		if( $url === 'TRUE' ){
			$post_parent_id = wp_get_post_parent_id($post_id);
			$url = get_post_meta( $post_parent_id, '_wp_attached_file', true );
		}

	}
 	return $url;
 }

function get_redirect_target($url){

    if( !function_exists('curl_version') ) return $url;
   
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $headers = curl_exec($ch);
    curl_close($ch);
    // Check if there's a Location: header (redirect)
    if (preg_match('/^Location: (.+)$/im', $headers, $matches))
        return trim($matches[1]);
    // If not, there was no redirect so return the original URL
    // (Alternatively change this to return false)
    return $url;
}
function get_element_string_by_xml($string)
{
    $dom = new DOMDocument();
    @$dom->loadHTML($string);
    $htmlString = $dom->saveHTML();
    return $htmlString;
}

function get_redirect_final_target($url){

     if( !function_exists('curl_version') ) return $url;
   
   
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // follow redirects
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // set referer on redirect
    curl_exec($ch);
    $target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_close($ch);
    if ($target)
        return $target;
    return $url;
}