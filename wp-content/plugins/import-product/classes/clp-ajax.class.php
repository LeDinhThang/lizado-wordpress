<?php
	class clp_Ajax {
		public function __construct(){
			add_action('init',array(&$this,'register_ajax') );
		}

		public function register_ajax(){
			//clp_admin_start
			add_action('wp_ajax_nopriv_clp_admin_start', array(&$this,'clp_admin_start'));
			add_action('wp_ajax_clp_admin_start', array(&$this,'clp_admin_start'));

			
		}
		

		function clp_admin_start(){
			$CPL_Data = new CLP_Data();
			$CPL_Data->_params  = $_POST;
			$CPL_Data->_files 	= $_FILES;
			$CPL_Data->start();
		}

		
	}

	$CLP_Ajax = new CLP_Ajax();