     <?php
        $options = get_option( 'clp_options' );
     
        if( !$options ){
            $options = array(
                'action','task','key','file','sheet','parrent_cat','upload_method'
            );
        }
        if( empty($options['upload_method']) ){
            $options['upload_method'] = 'ajax';
        }
    ?>
    <style type="text/css">
    #force {
        display: none;
    }
    .percent {
        color: white;
    }
    </style>

     <script>
       jQuery( document ).ready( function($){
            $('input[type="submit"]').attr('disabled', false);
            $('input[type="button"]').attr('disabled', false);
            $('#force').attr('checked', false);

            var cpl_ajax_url = '<?php echo home_url();?>/wp-admin/admin-ajax.php';
            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');
			
			var enable_ajax = '<?php echo $options['upload_method']; ?>';
			
			if( enable_ajax == 'ajax_upload' ){

                $('#upload_method_1').attr('checked', true);
                $('#upload_method_2').attr('checked', false);

				console.log('Using Ajax Method Upload File');
				$('form#import_form2').ajaxForm({
					beforeSend: function() {
						$('#start').val('Starting...');

						status.empty();
						var percentVal = '0%';
						bar.width(percentVal)
						percent.html(percentVal);
						write_to_log('Starting upload file...');
					},
					uploadProgress: function(event, position, total, percentComplete) {
						var percentVal = percentComplete + '%';
						bar.width(percentVal)
						percent.html(percentVal);
					},
					success: function() {

						var percentVal = '100%';
						bar.width(percentVal)
						percent.html(percentVal);
						
					},
					complete: function(xhr) {
						var respon = jQuery.parseJSON(xhr.responseText);
						
                        console.log( respon.status )

						if( respon.status == 1 ){
							var action  = respon.action;  
							var task    = respon.task;  
							var key     = respon.key;  
							var file    = respon.file; 
							var sheet   = respon.sheet;
							var parrent_cat   = respon.parrent_cat;
                            write_to_log('Upload success. Starting handle file.');
							cpl_process( action,task,key,file,sheet,parrent_cat );
						}else{
                            $('.txt_process').empty();
                            write_to_log( respon.msg );
                        }
                        if( typeof respon.sucess != 'undefined' && respon.sucess == 1 ){
                            $('input[type="submit"]').attr('disabled', false);
                            $('input[type="button"]').attr('disabled', false);
                            $('#force').attr('checked', false);
                            $('#start').val('Start');
                            $('#start_force').val('Start Force');
                        }

					}
				});
			}else{
				console.log('Using Nomal Method Upload File');
                $('#upload_method_1').attr('checked', false);
                $('#upload_method_2').attr('checked', true);
			}
			
            
			
			$('#save_setting').on('click',function(){
                $(this).val('Saving...');
				$('input[type="submit"]').attr('disabled', true);
                $('input[type="button"]').attr('disabled', true);
                var upload_method = $('input[name="upload_method"]:checked').val();
				var download_method = $('input[name="download_method"]:checked').val();
		
				 var dta = {
                    'action': 'clp_admin_start',
                    'task'  : 'save_setting',
                    'upload_method'  : upload_method,
                    'download_method'  : download_method,
                }    
                $.ajax({
                    type: "POST",
                    url: cpl_ajax_url,
                    data: dta,
                    dataType: "json"
                })
                .done(function( respon ) {
                    write_to_log(respon.msg);
                    if( respon.status == 1 ){
						$('#save_setting').val('Save Setting');
						$('input[type="submit"]').attr('disabled', false);
						$('input[type="button"]').attr('disabled', false);
						window.location.reload();
                    }
                    if( typeof respon.sucess != 'undefined' && respon.sucess == 1 ){
                        $('input[type="submit"]').attr('disabled', false);
                        $('input[type="button"]').attr('disabled', false);
                        $('#force').attr('checked', false);
                        $('#start').val('Start');
                        $('#start_force').val('Start Force');
                    }
                   
                });
            });

            // $('#start').on('click',function(){
                
            //     $('input[type="submit"]').attr('disabled', true);
            //     $('input[type="button"]').attr('disabled', true);
            // });

            $('#start_force').on('click',function(){
                $(this).val('Starting...');
                $('input[type="submit"]').attr('disabled', true);
                $('input[type="button"]').attr('disabled', true);
                $('#force').attr('checked', true);
                var dta = $( '#import_form2' ).serializeArray();
                $.ajax({
                    type: "POST",
                    url: cpl_ajax_url,
                    data: dta,
                    dataType: "json"
                }) .done(function( respon ) {
                    write_to_log(respon.msg);
                    if( respon.status == 1 ){
                        var action  = respon.action;  
                        var task    = respon.task;  
                        var key     = respon.key;  
                        var file    = respon.file; 
                        var sheet   = respon.sheet;
                        var parrent_cat   = respon.parrent_cat;
                        cpl_process( action,task,key,file,sheet,parrent_cat );
                    }
                     if( typeof respon.sucess != 'undefined' && respon.sucess == 1 ){
                        $('input[type="submit"]').attr('disabled', false);
                        $('input[type="button"]').attr('disabled', false);
                        $('#force').attr('checked', false);
                        $('#start').val('Start');
                        $('#start_force').val('Start Force');
                    }
                });
            });

            function cpl_process(action,task,key,file,sheet,parrent_cat){
                var dta = {
                    'action': action,
                    'task'  : task,
                    'key'   : key,
                    'file'  : file,
                    'sheet' : sheet,
                    'parrent_cat' : parrent_cat
                }    
                $.ajax({
                    type: "POST",
                    url: cpl_ajax_url,
                    data: dta,
                    dataType: "json"
                })
                .done(function( respon ) {
                    write_to_log(respon.msg);
                    if( respon.status == 1 ){
                        var action  = respon.action;  
                        var task    = respon.task;  
                        var key     = respon.key;  
                        var file    = respon.file; 
                        var sheet   = respon.sheet;
                        
                        var parrent_cat     = respon.parrent_cat;
                        cpl_process( action,task,key,file,sheet,parrent_cat );

                      
                    }
                    if( typeof respon.percent != 'undefined' ){
                        var percentVal      = respon.percent;
                        var bar = $('.bar');
                        var percent = $('.percent');
                        var percentVal = percentVal+'%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    }
                    if( typeof respon.done != 'undefined' && respon.done == 1 ){
                        alert('All Done !');
                    }
                    if( typeof respon.sucess != 'undefined' && respon.sucess == 1 ){
                        $('input[type="submit"]').attr('disabled', false);
                        $('input[type="button"]').attr('disabled', false);
                        $('#force').attr('checked', false);
                        $('#start').val('Start');
                        $('#start_force').val('Start Force');

                    }
                });

            }
            
            function write_to_log(text){
                 text = '<br>' + text;
                 $('.txt_process').prepend( text );
            }
        });
    </script>

    <?php if( isset( $_GET['force'] ) && $_GET['force'] == 1 ): ?>
    <script>
       jQuery( document ).ready( function($){
            $('#start_force').trigger('click');
       });
    </script>
    <?php endif; ?>

<?php 
	$action_url = '';
	if( $options['upload_method'] == 'ajax_upload' ){
		$action_url = home_url().'/wp-admin/admin-ajax.php';
	}
?>	
	
<form role="form" class="hideshow" id="import_form2" action="<?php echo $action_url;?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="action" value="clp_admin_start">
    <fieldset id="form_filedset">

        <div class="form-group">

            <label> Parrent Category: </label>
            <select class="form-control" name="parrent_cat">
                <option value="0">--Import as parrent category--</option>
                <?php
                    $taxonomies = array( 
                        'product_cat'
                    );
                    $args = array(
                        'orderby'           => 'name', 
                        'order'             => 'ASC',
                        'hide_empty'        => false, 
                        'exclude'           => array(), 
                        'exclude_tree'      => array(), 
                        'include'           => array(),
                        'number'            => '', 
                        'fields'            => 'all', 
                        'slug'              => '',
                        'parent'            => '',
                        'hierarchical'      => true, 
                        'child_of'          => 0,
                        'childless'         => false,
                        'get'               => '', 
                        'name__like'        => '',
                        'description__like' => '',
                        'pad_counts'        => false, 
                        'offset'            => '', 
                        'search'            => '', 
                        'cache_domain'      => 'core'
                    ); 
                    $woo_categories = get_terms($taxonomies, $args);
                    foreach ( $woo_categories as $woo_cat ) {
                        $woo_cat_id = $woo_cat->term_id; //category ID
                        $woo_cat_name = $woo_cat->name; //category name
                        $woo_cat_slug = $woo_cat->slug; //category slug

                        echo '<option value="'.$woo_cat_id.'">'.$woo_cat_name.'</option>';    
                    }
                ?>
            </select>

            
        </div>

        <div class="form-group">
        <label> File Import: </label>

            <input class="form-control" name="file_import" type="file" value="">
            <input class="form-control" name="task" type="hidden" value="upload_file">
        </div>

         <div class="form-group">

             <div class="progress">
                <div class="bar"></div >
                <div class="percent">0%</div >        
                 <div id="status"></div>         
            </div>
        </div>
        
         <div class="form-group">    
            <div style="clear:both;margin-top:10px;"></div>
            <div class="btnimportera">
                <input class="button11" type="submit" name="start" value="Start" id="start"/>
            </div>
            <div class="btnrensa">
                <?php if( @$options['file'] != '' ): ?>
                    <input class="button22" id="start_force" type="button" name="start_force" value="Start Force" />
                <?php endif;?>
            </div>
        </div>
    </fieldset>  

          
            <div class="panel panel-default">
                <div class="panel-body">  
                    <label>Last Action</label> 
                    <p>Enable Force Task. It means that you want to using the last step of the process</p>                                      
                    <div class="checkbox" style="display:none;">
                        <label>
                            <input id="force" type="checkbox" name="force" value="1">
                          Enable Force Task
                        </label>
                    </div>
                    <label>Task</label>  
                    <select class="form-control" name="task_force">
                        <option  value="read_file" <?php if ( @$options['task'] == 'read_file' ) echo 'selected';?> >read_file</option>    
                        <option  value="process_data" <?php if ( @$options['task'] == 'process_data' ) echo 'selected';?> >process_data</option>    
                    </select>
                        

                    <label>File</label>                              
                    <input class="form-control" id="page" name="file" type="text"value="<?php echo @$options['file'];?>">

                    <label>Key</label>                              
                    <input class="form-control" id="key" name="key" type="number" value="<?php echo @$options['key'];?>">
                   

                     <label>Sheet</label>                              
                    <input class="form-control" id="page" name="sheet" type="number"value="<?php echo @$options['sheet'];?>">
                   
                </div>  
            </div> 
			<div class="panel panel-default">
                <div class="panel-body">  
                    <label>Setting</label> 
                    <div class="form-group">
                        <div class="checkbox">
                            <p>Upload method:</p>
                            <label>
                                <input id="upload_method_1" type="radio" name="upload_method" value="ajax_upload" <?php if( @$options['upload_method'] == 'ajax_upload' ){ echo 'checked';} ?>>
                              Enable Ajax Upload
                            </label>
                             <label>
                                <input id="upload_method_2" type="radio" name="upload_method" value="normal" <?php if( @$options['upload_method'] == 'normal' ){ echo 'checked';} ?>>
                              Disable Ajax Upload
                            </label>
                        </div>
                    </div>
                    <div class="form-group hide">
                        <div class="checkbox">
                            <p>Image import:</p>
                            <label>
                                <input id="download_method_1" type="radio" name="download_method" value="cdn" <?php if( @$options['download_method'] == 'cdn' ){ echo 'checked';} ?>>
                              CDN
                            </label>
                             <label>
                                <input id="download_method_2" type="radio" name="download_method" value="download" <?php if( @$options['download_method'] == 'download' || @$options['download_method'] == ''){ echo 'checked';} ?>>
                              Download
                            </label>
                        </div>
                    </div>

                    

					 <div class="btnrensa">
						<input class="button22" id="save_setting" type="button" name="save_setting" value="Save Setting" />
					</div>
                </div>  
            </div>  	
</form>